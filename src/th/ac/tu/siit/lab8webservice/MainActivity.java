package th.ac.tu.siit.lab8webservice;
import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String,String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		setListAdapter(adapter);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		ConnectivityManager mgr = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			long current = System.currentTimeMillis();
			if (current - lastUpdate > 5*60*1000) {
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/bangkok.json");
			}
		}
		else {
			Toast t = Toast.makeText(this, 
					"No Internet Connectivity on this device. Check your setting", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String,String> record;
		ProgressDialog dialog;
		
		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}
		//Executed under thr UI thread before the task starts
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}
		//Executed under the UI thread after the task finished
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), 
					result, Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			//Set the title of the MainActivity
			setTitle("Bangkok Weather");
		}
		//Executed under the Background thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				//Get the first parameter string,and use it as a URL
				URL url = new URL(params[0]);
				//Create a connection to the URL
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				//Read data from the web server
				http.setDoInput(true);
				http.connect();
				
				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line);
					}
					//Extract values from the obtain data
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String,String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp));
					list.add(record);
					
					//Extract "description"
					//"weather" :[{ "description": "....."}]
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String description = w0.getString("description");
					record = new HashMap<String,String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Pressure");
					double press = jmain.getDouble("pressure");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", press));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "humidity");
					double hum = jmain.getDouble("humidity");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", hum));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Min");
					double min = jmain.getDouble("temp_min");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", min));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Max");
					double max = jmain.getDouble("temp_max");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", max));
					list.add(record);
					
					JSONObject jwind = json.getJSONObject("wind");
					
					record = new HashMap<String,String>();
					record.put("name", "Wind_Speed");
					double speed = jwind.getDouble("speed");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", speed));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Wind_deg");
					double deg = jwind.getDouble("deg");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", deg));
					list.add(record);
					
					return "Finished Loading Weather Data";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
	}

}

